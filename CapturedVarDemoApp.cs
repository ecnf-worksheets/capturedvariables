﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CapturedVariables
{
    public class CapturedVarDemoApp
    {
        public static void Main()
        {

            DemoCapturedVarWithLambda();
            DemoActionCapturedVarWithLambdaUsingLocalVars();

            // demonstrates the different behavior of the for and foreach loop with respect of 
            // iteration variables of the for/foreach loop (different execution context)
//            ForAndForeachAreNotTheSameDemo();

            Console.ReadKey();

        }

        /// <summary>
        /// Demonstrates the different behavior of the for and foreach loop with respect of 
        /// iteration variables of the for/foreach loop (different execution context; Closure)
        /// </summary>
        private static void ForAndForeachAreNotTheSameDemo()
        {
            // use for loop
            Console.WriteLine("For-Loop");
            var computes = new List<Func<int>>();
            for (int i = 0; i < 10; i++)
            {
                computes.Add(() => i*2);
            }
            PrintFunctions<int>(computes);

            Console.WriteLine("Foreach-Loop");
            computes = new List<Func<int>>();
            foreach (var i in Enumerable.Range(1, 10))
            {
                computes.Add(() => i*2);
            }
            PrintFunctions<int>(computes);

            Console.WriteLine("Linq-Query");
            computes = new List<Func<int>>();
            computes.AddRange(Enumerable.Range(1, 10).Select(i => (Func<int>) (() => i*2)));
            PrintFunctions<int>(computes);
        }

        /// <summary>
        /// Demonstrates the consequences of using lambdas with captured
        /// variables (Closures)
        /// </summary>
        static void DemoCapturedVarWithLambda()
        {
            Action[] actions = new Action[10];


            for(int j = 0; j < actions.Length; j++)
            {
                actions[j] = () => Console.WriteLine(j);
            }

            PrintActions(actions);
        }

        static void DemoActionCapturedVarWithLambdaUsingLocalVars()
        {
            Action[] actions = new Action[10];

            
            for (int i = 0; i < actions.Length; i++)
            {
                int innerI = i;
                actions[i] = () => Console.WriteLine(innerI);
            }

            PrintActions(actions);
        }

        static void PrintActions(Action[] actions)
        {
            foreach (Action a in actions)
            {
                a();
            }
            Console.WriteLine();

        }


        static void PrintFunctions<T>(IEnumerable<Func<int>> funcs)
        {
            foreach (var f in funcs)
            {
                Console.WriteLine(f());
            }

        }

    }


}
